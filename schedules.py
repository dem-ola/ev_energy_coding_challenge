import json
from datetime import datetime, timezone, timedelta
from tariffs import tariffs
from operator import itemgetter

def minutes_difference(end, start):
	# check minutes difference between two timestamps
	if start >= end: return 0
	return (end - start).seconds // 60

def get_ready_by(plugged, ready_by):
	''' create time to be ready by 
		this is provided in string HH:MM format
	'''

	set_next_day = False
	
	ready_mins = int(ready_by[-2:])
	ready_hour = int(ready_by[:2])
	ready_day = plugged.day

	# move year and month along if end of year/month
	# not shown below due to time constraints
	ready_year = plugged.year
	ready_month = plugged.month
	
	# move day along one if to complete is next day
	plug_hour = plugged.hour
	if ready_hour < plug_hour:
		set_next_day = True

	if set_next_day:
		ready_day += 1 

	by_ = datetime(ready_year, ready_month, ready_day, 
					hour=ready_hour, minute=ready_mins, 
					tzinfo=plugged.tzinfo)
	
	return by_

def epoch_offset(ready, offset, action='subtract', tolerance=600):
	''' calculate last time we can charge 
		we use epoch (converting all to seconds) 
		to make calculation easier
		we'll add an offset tolerance so car is ready
		x secs before needed default (10mins)
	'''
	epoch = int(ready.strftime('%s'))
	if action == 'add': offset = -offset
	off_ = epoch - offset - tolerance
	off_str = datetime.fromtimestamp(off_).strftime('%Y-%m-%dT%H:%M')
	return (off_, off_str)

def schedules(tariff, ready_by, charge_time, plug_in_time):
	''' calculate schedules given args and save to json 
		returns tuple: 
		(earliest charge start - time on application server, 
		latest charge start on server, 
		latest charge start in customer's local time)
		though latest server start time preferred but there might
		be other reasons to start earlier

		warning: this function does not as at now work with 
		negative utc offsets. The sign can be determined using regex
		but this isn't shown
	'''

	# raise error if no valid tariff supplied
	try:
		tar = tariffs[tariff]
	except:
		raise Exception('Please provide a valid tariff')

	# get future ready time
	# convert plug_in into datetime object and then 
	# call function to get time it must be ready
	# nb assumption that this function is called 
	# at the time the customer plugs in
	plugged = datetime.fromisoformat(plug_in_time)
	ready = get_ready_by(plugged, ready_by)

	# check how many minutes available
	# we should have enough time to charge
	minutes = minutes_difference(ready, plugged)
	if minutes < charge_time:
		return None

	# the latest time we can start charging - customer local time
	# ready_by - charge_time
	latest, latest_str = epoch_offset(ready, charge_time * 60)

	#convert this into local time on application server
	now = datetime.now()
	utc = int(str(plugged.utcoffset()).split(':')[0]) # utc hours
	latest_ = datetime.fromtimestamp(latest)
	serv_latest, serv_str = epoch_offset(latest_, utc * 60 * 60, 'subtract', 0)

	# this means the car can be on between now and latest_start
	now = now.isoformat()
	on = (now, serv_str, latest_str)

	return on

sched = schedules('go', '07:00', 60, '2019-11-10T18:42:12+02:00')
print()
print(sched)
print('-'*40)
print()


# ========  OPTIMISATION ======================

# for optimisations
# we open json file with context manager to ensure file closed whatever happens
# data is loaded into a dictionary
with open('carbon_intensity_json.json') as jf:
    j_dict = json.load(jf)

# assuming that a low carbon intensity is preferred
# and that index = price; actual=carbon intensity
# we sort first by price and then by index
# 'forecast' readings used instead of 'actual'
# on the basis that this will be the best estimate at the time
# a decision is made: assumption is forecast algorithm is continuously improved
# probably useful to check the correlation between forecast and actual
# to see if this assumption is reasonable
carbs = []
intense_list = j_dict['data']

for item in intense_list:
	carbs.append((item['from'], item['intensity']['index'], 
					item['intensity']['forecast']))

carbs_sorted = sorted(carbs, key=itemgetter(2,1))
print(carbs_sorted)

# not completed but from our sorted list of tuples we'll need
# to identify the best time frames to charge given the charge
# times we identified earlier

'''

		.... code


''' 

