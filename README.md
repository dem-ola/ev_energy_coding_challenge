# ev.energy coding challenge


### MY SUBMISSION

You should be able to run this from a command line

% python schedules.py

This will output two sections of print:

1. a tuple of charge time ranges
 (earliest time to start, 
  latest time to start - server,
  latest time to start - customer local time)

  Currently code only handles UTC+ times and not UTC-


2. a sorted list of tuples 
	(start half-hour charge time frame,
	price - low/moderate/high - primary sort,
	intensity - 'forecast' - secondary sort)

	This will then have to be traversed to check best times
	to charge eg if only a one hour charge is required then 
	it might be better to split the charge into two separate
	time frames that may not be contiguous. This is not 
	shown in the code.


### FINITO




## Context

### Guidance on your submission

The problem below is designed to be suitable for all ability levels, but it's not designed to be completed in full. Do not panic if you don't feel you've made progress, just write down your thoughts 😀

Craft your response to this question as if it were going in to the main repository i.e.

- fork this repo (privately please!)
- commit code using whatever strategy you favour or feel is appropriate
- think about what you would do next if this were more than one day's work
- imagine that your submission were a pull request (but don't actually make a PR...)

Please don't spend too much time writing code

- Only spend 2-3 hours on this; your time is valuable and we don't want to waste it
- It's practically impossible to come up with a complete solution to the problem in 2-3 hours
- *trust me, we've been spending years on it!*

It's fine to not finish the functionality - pseudocode is valid, provided we can have a conversation about it

### Feedback

Thank you for taking part in the coding challenge. Whilst the coding challenge is part of the interview process, we are always looking for feedback. If you would like to provide feedback, please e-mail Will Ruddick <will.ruddick@ev.energy>, the current owner of the challenge.

### About ev.energy and what we do

At ev.energy we smart charge electric cars to use cheaper, cleaner energy without impacting convenience for the end user. That means we have to choose when to turn on and off charging by creating schedules for charging cars. At the core of our platform is an algorithm which takes in various parameters such as the likely carbon intensity of electricity, the price of energy from certain suppliers and balances this with the needs of the user; such as when they need their car ready by, and the charge level of their battery. Your challenge is to write a stripped down scheduler which takes in these parameters and presents as sensible schedule for us to charge our customer's car.

## The Problem Statement

Enid has a Tesla Model X P90D which she wants to smart charge using the ev.energy platform. Enid's power is supplied by Octopus and she is on the 'Go' tariff, which provides 7 hours of off-peak charging from 00:30 - 07:30 local time. She always pops down to the shop at 7.30am and so would like her car ready by 7am. She comes home and plugs her car in on the evening of 27th September 2019.

Given an amount of time needed to charge Enid’s car (an integer of minutes) and plug in time (time in isoformat) output a schedule which can be used to smart charge her car. The schedule should be a list of times when the car should be on or off.

Example:

- Ready by time - 7.00am (user’s timezone)
- Charge time - 300 (minutes)
- Plug in time - 2019-10-04T18:42:12+00:00

For example, a call to your function might look like:

```python
your_function(ready_by='07:00', charge_time=300, plug_in_time='2019-10-04T18:42:12+00:00')
```

### Stretch questions

- We prioritise price first and then carbon intensity - a JSON file is provided of carbon intensity for Enid's region which should be prioritised as a secondary optimisation
- Enid lives up North where it gets pretty cold! She knows that her car is more efficient if it has been charging in the hour before her ready by time. So ideally the hour before her ready by time the car will charge.
- It would be nice if Enid moved to spain it would still work - so timezone aware schedules would help

## Deliverables

Please invite Chris (chrisdarby11 on Bitbucket) to view your repository (and update the Readme with your own notes / the description you would put in a pull request). Pleae **do not** try to make a Pull Request back to the main repo. Please e-mail Chris <chris.darby@ev.energy> once you're done.