''' tariffs 

	only used to check if tariff given is in the dictionary
	could be developed further in other ways if necessary
'''

tariffs = {
	'go': {
		'off_peak_hours': 7,
		'start_off': '00:30',
		'end_off': '07:30'
	}
}